<h1 align="center">Welcome to Personal Website 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-2.1-blue.svg?cacheSeconds=2592000" />
  <a href="https://github.com/isyuricunha/website" target="_blank">
    <img alt="Documentation" src="https://img.shields.io/badge/documentation-yes-brightgreen.svg" />
  </a>
  <a href="https://github.com/isyuricunha/website/blob/main/license.md" target="_blank">
    <img alt="License: Personal License" src="https://img.shields.io/badge/License-Personal License-yellow.svg" />
  </a>
  <a href="https://twitter.com/isyuricunha" target="_blank">
    <img alt="Twitter: isyuricunha" src="https://img.shields.io/twitter/follow/isyuricunha.svg?style=social" />
  </a>
</p>

> I use this to show some of my projects off, and test things out. You can see the websites & tools that I like. Sometimes I even write blogs.

### 🏠 [Homepage](https://yuricunha.com/)

### ✨ [Demo](https://yuricunha.com/)

## Install

```sh
yarn
```

## Usage

```sh
yarn dev
```

## Run tests

```sh
yarn dev
```

## Author

👤 **https://www.github.com/isyuricunha**

- Website: https://yuricunha.com/
- Twitter: [@isyuricunha](https://twitter.com/isyuricunha)
- Github: [@isyuricunha](https://github.com/isyuricunha)
- LinkedIn: [@isyuricunha](https://linkedin.com/in/isyuricunha)

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://github.com/isyuricunha/website/issues).

## Show your support

Give a ⭐️ if this project helped you!

<a href="https://www.patreon.com/isyuricunha">
  <img src="https://c5.patreon.com/external/logo/become_a_patron_button@2x.png" width="160">
</a>

## 📝 License

Copyright © 2023 [https://www.github.com/isyuricunha](https://github.com/isyuricunha).<br />
This project is [Personal License](https://github.com/isyuricunha/website/blob/main/license.md) licensed.

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
